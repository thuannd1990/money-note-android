package Models.BusinessModel

import Models.FirebaseModels.FBTransactionEntity

class BTransactionModel {

    var uid: String = ""
    var money: Double = 0.0
    var transaction_fee: Double = 0.0
    var category_uid: String = ""
    var wallet_uid: String = ""
    var to_wallet_uid: String = ""
    var note: String = ""
    var date: Long = 0
    var day: String = ""
    var month: String = ""
    var year: String = ""
    var transactionType: TRANSACTION_TYPE = TRANSACTION_TYPE.UNKNOW
    var forPersonUid: String = ""
    var debtloan_transaction_uid: String? = ""
    var event_uid: String = ""
    var isCanReport: Boolean = true

    constructor(entity: FBTransactionEntity) {
        uid                 = entity.uid
        money               = entity.money.toDouble()

        if (entity.transaction_fee.toDoubleOrNull() != null) {
            transaction_fee     = entity.transaction_fee.toDouble()
        }

        category_uid        = entity.category_uid
        wallet_uid          = entity.wallet_uid
        to_wallet_uid       = entity.to_wallet_uid
        note                = entity.note

        if (entity.date.toDoubleOrNull() != null) {
            date            = entity.date.toLong()
        }

        day                 = entity.day
        month               = entity.month
        year                = entity.year
        transactionType     = Utils.transactionType(entity.transactionType)
        debtloan_transaction_uid = entity.debtloan_transaction_uid
        event_uid           = entity.event_uid

        if (entity.isCanReport == "0") {
            isCanReport = false
        }
    }
}