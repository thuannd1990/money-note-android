package Models.BusinessModel

import Models.FirebaseModels.FBTransactionEntity
import Models.FirebaseModels.FBWalletEntity
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class BWalletModel {
    var uid: String = ""
    var name: String = ""
    var currencyName: String = ""
    var currencyCode: String = ""
    var currencySymbol: String = ""
    var countryName: String = ""
    var countryCode: String = ""

    var currentMoney: Double = 0.0

    var listMonth: MutableList<BWalletMonthModel> = mutableListOf()

    constructor(entity: FBWalletEntity) {
        uid             = entity.uid
        name            = entity.name
        currencyName    = entity.currencyName
        currencyCode    = entity.currencyCode
        currencySymbol  = entity.currencySymbol
        countryCode     = entity.countryCode
        countryName     = entity.countryName

        setUpSyncData()
    }

    private fun setUpSyncData() {
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            return
        }

        val database = Firebase.database
        val ref = database.getReference(currentUser.uid)
        val refTableWallet = ref.child(FBTableType.tableTransaction)
        refTableWallet.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(p0: DataSnapshot) {

                var listTransaction: MutableList<BTransactionModel> = mutableListOf()
                var money: Double = 0.0

                for (snapshot in p0.children) {
                    val entity = FBTransactionEntity(snapshot)

                    if (entity.wallet_uid == uid || entity.to_wallet_uid == uid) {
                        val model = BTransactionModel(entity)
                        Log.d("BWalletModel", "onDataChange money: ${entity.money}")

                        if (model.transactionType == TRANSACTION_TYPE.EXPENSE) {
                            money -= model.money

                        } else if (model.transactionType == TRANSACTION_TYPE.IN_COME) {
                            money += model.money

                        } else if (model.transactionType == TRANSACTION_TYPE.TRANSFER_WALLET) {
                            if (entity.wallet_uid == uid) {
                                money -= model.money
                                money -= model.transaction_fee

                            } else if (entity.to_wallet_uid == uid) {
                                money += model.money
                            }
                        } else if (model.transactionType == TRANSACTION_TYPE.UPDATE_WALLET_BALANCE) {
                            money += model.money
                        }

                        listTransaction.add(model)
                    }
                }

                currentMoney = money
            }

            override fun onCancelled(p0: DatabaseError) {

            }
        })
    }

    private fun addTransaction(item: BTransactionModel) {

    }
}