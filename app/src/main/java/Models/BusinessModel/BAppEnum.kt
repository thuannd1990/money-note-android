package Models.BusinessModel

enum class TRANSACTION_TYPE {
    EXPENSE,
    IN_COME,
    DEBT_LOAN,
    UPDATE_WALLET_BALANCE,
    TRANSFER_WALLET,
    UNKNOW
}

enum class DEBT_LOAN_TYPE {
    CHO_VAY,
    DI_VAY,
    TRA_NO,
    THU_NO,
    NONE,
}

enum class MONEY_TYPE {
    IN_COME,
    OUT_COME,
    UNKNOW
}