package Models.BusinessModel

import Models.FirebaseModels.*
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class FBTableType {
    companion object {
        const val tableWallet: String       = "TFBWalletDataTable"
        const val tableTransaction: String  = "TFBTransactionDataTable"
        const val tableCategory: String     = "TFBCategoryDataTable"
        const val tableContact: String      = "TFBPersonDepLoanDataTable"
        const val tableEvent: String        = "TFBEventDataTable"
    }
}

class LocalBroadCastType {
    companion object {
        const val eventUpdateListWallet: String = "eventUpdateListWallet"
        const val eventUpdateWallet: String = "eventUpdateListWallet"
        const val eventUpdateEvent: String = "eventUpdateListWallet"
    }
}

interface AppDataListener {
    // These methods are the different events and
    // need to pass relevant arguments related to the event triggered
    fun didUpdateListWallet(list: List<BWalletModel>)
    fun ddiUpdateWallet(wallet: BWalletModel)
}

public final class AppDataManager {

    companion object {
        var shared: AppDataManager = AppDataManager()
    }

    var context: Context? = null
    var boardCastLocal: LocalBroadcastManager? = null

    var mapWalletEntity: MutableMap<String, FBWalletEntity> = mutableMapOf()
    var mapWallet: MutableMap<String, BWalletModel> = mutableMapOf()

    var arrCategoryEntity: MutableList<FBCategoryEntity> = mutableListOf()
    var arrTransactionEntity: MutableList<FBTransactionEntity> = mutableListOf()
    var arrEventEntity: MutableList<FBEventEntity> = mutableListOf()
    var arrContactEntity: MutableList<FBContactEntity> = mutableListOf()

    private var refTableWallet:  DatabaseReference? = null
    private var refTableCategory:  DatabaseReference? = null
    private var refTableTransaction:  DatabaseReference? = null
    private var refTableEvent:  DatabaseReference? = null
    private var refTableContact:  DatabaseReference? = null

    fun setUp() {
        Log.d("AppDataManager","getAllData")
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            return
        }

        if (context != null) {
            boardCastLocal = LocalBroadcastManager.getInstance(context!!)
        }

        val database = Firebase.database
        val ref = database.getReference(currentUser.uid)
        refTableWallet      = ref.child(FBTableType.tableWallet)
        refTableCategory    = ref.child(FBTableType.tableCategory)
        refTableTransaction = ref.child(FBTableType.tableTransaction)
        refTableEvent       = ref.child(FBTableType.tableEvent)
        refTableContact     = ref.child(FBTableType.tableContact)

        doSetUpCategoryTable()
        doSetUpWalletTable()
        doSetUpContactTable()
        doSetUpEventTable()
        doSetUpTransactionTable()
    }

    private fun doSendEvent(type: LocalBroadCastType) {

    }

    //MARK:- TABLE WALLET
    private fun doSetUpWalletTable() {
        refTableWallet?.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {
                val entity = FBWalletEntity(dataSnapshot)
                mapWalletEntity.put(entity.uid, entity)

                val model = BWalletModel(entity)
                mapWallet.put(model.uid, model)

                val intent = Intent()
                intent.action = LocalBroadCastType.eventUpdateListWallet
                boardCastLocal?.sendBroadcast(intent)
                Log.d("addChildEventListener", "onChildAdded: ${mapWallet.count()}")
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
                val entity = FBWalletEntity(dataSnapshot)
                mapWalletEntity.put(entity.uid, entity)

                val model = BWalletModel(entity)
                mapWallet.put(model.uid, model)

                val intent = Intent()
                intent.action = LocalBroadCastType.eventUpdateListWallet
                boardCastLocal?.sendBroadcast(intent)
                boardCastLocal?.sendBroadcastSync(intent)
                Log.d("addChildEventListener", "onChildChanged: $boardCastLocal - " + mapWallet.count())
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                mapWallet.remove(dataSnapshot.key.toString())

                val intent = Intent()
                intent.action = LocalBroadCastType.eventUpdateListWallet
                boardCastLocal?.sendBroadcast(intent)
                Log.d("addChildEventListener", "onChildRemoved:" + mapWallet.count())
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
                Log.d("addChildEventListener", "onChildMoved:" + dataSnapshot.key!!)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.w("addChildEventListener", "postComments:onCancelled", databaseError.toException())
            }
        })
    }

    //MARK:- TABLE CATEGORY
    private fun doSetUpCategoryTable() {

    }

    //MARK:- TABLE TRANSACTION
    private fun doSetUpTransactionTable() {

    }

    //MARK:- TABLE EVENT
    private fun doSetUpEventTable() {

    }

    //MARK:- TABLE CONTACT
    private fun doSetUpContactTable() {

    }
}

private fun <K, V> Map<K, V>.set(index: K, value: V) {

}
