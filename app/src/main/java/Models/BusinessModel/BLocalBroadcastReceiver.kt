package Models.BusinessModel

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class BLocalBroadcastReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d("BLocalBroadcastReceiver", "onReceive ${intent.action}")
    }
}