package Models.BusinessModel

import java.text.DecimalFormat
import java.text.NumberFormat


class Utils {

    companion object {
        fun currencyFormat(number: Double): String {
            val formatter: NumberFormat = DecimalFormat("#,###")
            val formattedNumber = formatter.format(number)
            return formattedNumber
        }

        fun transactionType(str: String): TRANSACTION_TYPE {
            if (str == "outcome") {
                return TRANSACTION_TYPE.EXPENSE
            }

            if (str == "income") {
                return TRANSACTION_TYPE.IN_COME
            }

            if (str == "transferWallet") {
                return TRANSACTION_TYPE.TRANSFER_WALLET
            }

            if (str == "updateWalletBalance") {
                return TRANSACTION_TYPE.UPDATE_WALLET_BALANCE
            }
            return TRANSACTION_TYPE.UNKNOW
        }
    }
}