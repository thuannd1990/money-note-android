package Models.FirebaseModels

import com.google.firebase.database.DataSnapshot

class FBContactEntity {

    var uid: String = ""
    var contactIdentifier: String = ""
    var name: String = ""

    constructor(snapshot: DataSnapshot) {
        val dictionary: Map<String, String> = snapshot.value as Map<String, String>

        this.uid                = snapshot.key.toString()
        this.contactIdentifier  = dictionary.get("contactIdentifier").toString()
        this.name               = dictionary.get("name").toString()
    }
}