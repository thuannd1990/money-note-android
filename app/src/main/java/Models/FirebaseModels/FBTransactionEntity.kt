package Models.FirebaseModels

import com.google.firebase.database.DataSnapshot

class FBTransactionEntity {

    var uid: String = ""
    var money: String = ""
    var transaction_fee: String = ""
    var category_uid: String = ""
    var wallet_uid: String = ""
    var to_wallet_uid: String = ""
    var note: String = ""
    var date: String = ""
    var day: String = ""
    var month: String = ""
    var year: String = ""
    var transactionType: String = ""
    var forPersonUid: String = ""
    var debtloan_transaction_uid: String = ""
    var event_uid: String = ""
    var isCanReport: String = ""

    constructor(snapshot: DataSnapshot) {
        val dict: Map<String, String> = snapshot.value as Map<String, String>
        uid                 = snapshot.key.toString()
        money               = dict["money"].toString()
        transaction_fee     = dict["transaction_fee"].toString()
        category_uid        = dict["category_uid"].toString()
        wallet_uid          = dict["wallet_uid"].toString()
        to_wallet_uid       = dict["to_wallet_uid"].toString()
        note                = dict["note"].toString()
        date                = dict["date"].toString()
        day                 = dict["day"].toString()
        month               = dict["month"].toString()
        year                = dict["year"].toString()
        transactionType     = dict["transactionType"].toString()
        forPersonUid        = dict["forPersonUid"].toString()
        debtloan_transaction_uid = dict["debtloan_transaction_uid"].toString()
        event_uid           = dict["event_uid"].toString()
        isCanReport         = dict["isCanReport"].toString()
    }
}