package Models.FirebaseModels

import com.google.firebase.database.DataSnapshot

class FBEventEntity {

    var uid: String = ""
    var name: String = ""
    var note: String = ""

    constructor(snapshot: DataSnapshot) {
        val dictionary: Map<String, String> = snapshot.value as Map<String, String>

        this.uid    = snapshot.key.toString()
        this.name   = dictionary.get("name").toString()
        this.note   = dictionary.get("note").toString()
    }
}