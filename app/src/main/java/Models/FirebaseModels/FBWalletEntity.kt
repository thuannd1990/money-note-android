package Models.FirebaseModels

import com.google.firebase.database.DataSnapshot

class FBWalletEntity {
    var uid: String = ""
    var name: String = ""
    var currencyName: String = ""
    var currencyCode: String = ""
    var currencySymbol: String = ""
    var countryName: String = ""
    var countryCode: String = ""

    constructor(snapshot: DataSnapshot) {
        val dictionary: Map<String, String> = snapshot.value as Map<String, String>

        this.uid            = snapshot.key.toString()
        this.name           = dictionary.get("name").toString()
        this.currencyName   = dictionary.get("currencyName").toString()
        this.currencyCode   = dictionary.get("currencyCode").toString()
        this.currencySymbol = dictionary.get("currencySymbol").toString()
        this.countryName    = dictionary.get("countryName").toString()
        this.countryCode    = dictionary.get("countryCode").toString()
    }
}