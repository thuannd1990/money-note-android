package Models.FirebaseModels

import com.google.firebase.database.DataSnapshot

class FBCategoryEntity {

    var uid: String = ""
    var name: String = ""
    var parentCategoryUid: String = ""
    var type: String = "" // debt_loan | expense | income
    var transactionType: String = "" // outcome | income
    var depLoanType: String = "" // cho_vay | di_vay | tra_no | thu_no |

    constructor(snapshot: DataSnapshot) {
        val dictionary: Map<String, String> = snapshot.value as Map<String, String>

        uid                 = snapshot.key.toString()
        name                = dictionary["name"].toString()
        parentCategoryUid   = dictionary["parentCategoryUid"].toString()
        type                = dictionary["type"].toString()
        transactionType     = dictionary["transactionType"].toString()
        depLoanType         = dictionary["depLoanType"].toString()
    }
}