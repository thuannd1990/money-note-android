package com.example.moneynote.TabbarHomePage

import Models.BusinessModel.AppDataManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.moneynote.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import kotlinx.android.synthetic.main.activity_tabbar_home_page.*
import com.google.firebase.ktx.Firebase

class TabbarHomePageActivity : AppCompatActivity() {

    lateinit var historyFragment: FragmentHistory
    lateinit var monthReportFragment: FragmentMonthReport
    lateinit var listWalletFragment: FragmentListWallet
    lateinit var reportHomePageFragment: FragmentReportHome
    lateinit var moreFragment: FragmentMore
    var dataManager: AppDataManager = AppDataManager.shared

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        supportActionBar?.hide()
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
//        supportActionBar?.title = "test"
        setContentView(R.layout.activity_tabbar_home_page)

        historyFragment = FragmentHistory()

        Firebase.database.setPersistenceEnabled(true)
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) Firebase.database.getReference(currentUser.uid).keepSynced(true)

        dataManager.context = this.applicationContext
        dataManager.setUp()

        supportFragmentManager.beginTransaction()
            .replace(R.id.frameLayout, historyFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        bottomTabbar.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.tabHistory -> {
                    historyFragment = FragmentHistory()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, historyFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit()
                }

                R.id.tabMonth -> {
                    monthReportFragment = FragmentMonthReport()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, monthReportFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit()
                }

                R.id.tabWallets -> {
                    listWalletFragment = FragmentListWallet()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, listWalletFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit()
                }

                R.id.tabReports -> {
                    reportHomePageFragment = FragmentReportHome()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, reportHomePageFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit()
                }

                R.id.tabUtil -> {
                    moreFragment = FragmentMore()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.frameLayout, moreFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit()
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }
}
