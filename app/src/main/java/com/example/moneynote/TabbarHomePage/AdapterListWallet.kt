package com.example.moneynote.TabbarHomePage

import Models.BusinessModel.*
import Models.FirebaseModels.FBWalletEntity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.example.moneynote.R
import kotlinx.android.synthetic.main.cell_wallet.view.*

class AdapterListWallet: BaseAdapter {

    var myContext: Context
    var localBroadcast : LocalBroadcastManager
    var myLayout: Int =  R.layout.cell_wallet
    var listWallet: MutableList<BWalletModel> = mutableListOf()

    val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            Log.d("AdapterListWallet", "onReceive ${intent?.action}")
            listWallet = AppDataManager.shared.mapWallet.values.toMutableList()
            listWallet.sortBy { item -> item.name }
            notifyDataSetChanged()
        }
    }

    constructor(context: Context) {
        myContext = context
        localBroadcast = LocalBroadcastManager.getInstance(context)
        listWallet = AppDataManager.shared.mapWallet.values.toMutableList()
        listWallet.sortBy { item -> item.name }
        Log.d("AdapterListWallet", "init")

        LocalBroadcastManager.getInstance(context).registerReceiver(broadCastReceiver, IntentFilter(LocalBroadCastType.eventUpdateListWallet));
    }

    override fun getCount(): Int {
        return listWallet.count()
    }

    override fun getItem(position: Int): BWalletModel {
        return listWallet[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater: LayoutInflater = myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(myLayout, null)

        //ánh xạ và gán giá trị
        val item = getItem(position)
        val lblName: TextView = view.findViewById(R.id.lblWalletName)
        lblName.text = item.name

        val lblMoney: TextView = view.findViewById(R.id.lblWalletMoney)
        lblMoney.text = "${Utils.currencyFormat(item.currentMoney)} ${item.currencySymbol}"
        return view
    }
}