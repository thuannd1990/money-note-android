package com.example.moneynote.Authent.ForgotPass

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.moneynote.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.txfEmail

class Register : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        btnSend.setOnClickListener { this.btnRegisterPressed() }
    }

    fun btnRegisterPressed() {
        if (this.isInvalidData()) {
            this.doRegister()
        }
    }

    fun isInvalidData(): Boolean {
        val email = txfEmail.text.toString()
        val pass1 = txfPassword.text.toString()
        val pass2 = txfRePassword.text.toString()

        if (email.isEmpty()) {
            val toast = Toast.makeText(applicationContext, "Email invalid", Toast.LENGTH_SHORT)
            toast.show()
            return false
        }

        if (pass1.isEmpty() || pass2.isEmpty()) {
            val toast = Toast.makeText(applicationContext, "Input password, please!", Toast.LENGTH_SHORT)
            toast.show()
            return false
        }

        if (!pass1.equals(pass2)) {
            val toast = Toast.makeText(applicationContext, "Password missmatch.", Toast.LENGTH_SHORT)
            toast.show()
            return false
        }
        return true
    }

    fun doRegister() {
        val email = txfEmail.text.toString()
        val pass1 = txfPassword.text.toString()

        val auth = FirebaseAuth.getInstance()
        auth.createUserWithEmailAndPassword(email, pass1).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d("doRegister", "register success")

            } else {
                val toast = Toast.makeText(applicationContext, task.exception?.message, Toast.LENGTH_SHORT)
                toast.show()
            }
        }
    }
}
