package com.example.moneynote.Authent.ForgotPass

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.moneynote.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_forgot_pass.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.btnSend
import kotlinx.android.synthetic.main.activity_register.txfEmail

class ForgotPass : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)
        btnSend.setOnClickListener { doSendForgotPass() }
    }

    fun doSendForgotPass() {
        val email = txfEmail.text.toString()

        if (email.isEmpty()) {
            val toast = Toast.makeText(applicationContext, "Email invalid", Toast.LENGTH_SHORT)
            toast.show()
            return
        }

        val auth = FirebaseAuth.getInstance()
        auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if (task.isComplete) {
                val toast = Toast.makeText(applicationContext, "Sent", Toast.LENGTH_SHORT)
                toast.show()
                finish()
            }
        }
    }
}
