package com.example.moneynote.Authent.Login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.moneynote.R
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.DoubleBounce
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*


class Login : AppCompatActivity() {

    var presenter: LoginPresenter = LoginPresenter()
    private var loginManager: LoginManager = LoginManager.getInstance()
    private var callbackManager: CallbackManager = CallbackManager.Factory.create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        supportActionBar?.hide()
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_login)
        setUpView()
        presenter.onCreate()
    }

    fun setUpView() {
        val router = LoginRouter()
        router.activity = this
        presenter.router = router

        btnForgot.setOnClickListener({ this.presenter.doGotoForgotPasswordScreen() })
        btnRegister.setOnClickListener({ this.presenter.doGoRegisterScreen() })
        btnLogin.setOnClickListener({ btnLoginPressed() })
        btnLoginFacebook.setOnClickListener( { btnFacebookPressed() })
    }

    fun progressBar(): ProgressBar {
        return findViewById<View>(R.id.spin_kit) as ProgressBar
    }

    fun btnLoginPressed() {
        val email = txfEmail.text.toString()
        val pass = txfPass.text.toString()

        if (email.isEmpty()) {
            val toast = Toast.makeText(applicationContext, "Email invalid", Toast.LENGTH_SHORT)
            toast.show()
            return
        }

        if (pass.isEmpty()) {
            val toast = Toast.makeText(applicationContext, "Password invalid", Toast.LENGTH_SHORT)
            toast.show()
            return
        }

        progressBar().visibility = View.VISIBLE
        this.presenter.doLogin(txfEmail.text.toString(), txfPass.text.toString())
    }

    fun btnFacebookPressed() {
        loginManager.logInWithReadPermissions(this, Arrays.asList("email", "public_profile"))

        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("doLoginFacebook", "onSuccess");
                presenter.doLoginFirebaseWithResult(loginResult);
            }

            override fun onCancel() {
                Log.d("doLoginFacebook", "onCancel");
                showErr("Canceled")
            }

            override fun onError(error: FacebookException) {
                Log.d("doLoginFacebook", "error: ${error.message.toString()}");

                showErr(error.message.toString())
            }
        })
    }

    fun hideProgressHub() {
        progressBar().visibility = View.INVISIBLE
    }

    fun showErr(err: String) {
        progressBar().visibility = View.INVISIBLE
        val toast = Toast.makeText(applicationContext, err, Toast.LENGTH_SHORT)
        toast.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("onActivityResult", "onActivityResult abc")
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }
}
