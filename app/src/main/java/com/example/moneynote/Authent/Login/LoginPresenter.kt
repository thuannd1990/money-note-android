package com.example.moneynote.Authent.Login
import android.content.Intent
import android.util.Log
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import java.util.*


class LoginPresenter {

    var router: LoginRouter? = null
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private var loginManager: LoginManager = LoginManager.getInstance()
    private lateinit var callbackManager: CallbackManager

    fun onCreate() {
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) {
            this.router?.doGoHomePage()
        }
    }

    fun doGotoForgotPasswordScreen() {
        this.router?.doGotoForgotPasswordScreen()
    }

    fun doGoRegisterScreen() {
        this.router?.doGoRegisterScreen()
    }

    fun doLogin(email: String, pass: String) {
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user =  auth.currentUser
                    if (user != null) {
                        this.router?.doGoHomePage()

                    } else {
                        Log.d("LoginPresenter", "Failed 0: ${task.exception}")
                        this.router?.showToast(task.exception?.message.toString())
                    }

                } else {
                    Log.d("LoginPresenter", "Failed 1: ${task.exception}")
                    this.router?.showToast(task.exception?.message.toString())
                }
            }
    }

    fun doLoginFacebook() {
        if (this.router?.activity == null) {
            return
        }

        loginManager.logInWithReadPermissions(
            this.router?.activity,
            Arrays.asList("email", "public_profile")
        )

        callbackManager = CallbackManager.Factory.create()

        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("doLoginFacebook", "onSuccess");
                doLoginFirebaseWithResult(loginResult);
            }

            override fun onCancel() {
                Log.d("doLoginFacebook", "onCancel");
            }

            override fun onError(error: FacebookException) {
                Log.d("doLoginFacebook", "error: ${error.message.toString()}");
            }
        })
    }

    fun doLoginFirebaseWithResult(result: LoginResult) {
        val token = result.accessToken
        val credential = FacebookAuthProvider.getCredential(token.token)

        auth.signInWithCredential(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val user =  auth.currentUser
                if (user != null) {
                    this.router?.doGoHomePage()

                } else {
                    Log.d("LoginPresenter", "Failed 0: ${task.exception}")
                    this.router?.showToast(task.exception?.message.toString())
                }

            } else {
                Log.d("LoginPresenter", "Failed 1: ${task.exception}")
                this.router?.showToast(task.exception?.message.toString())
            }
        }
    }
}
