package com.example.moneynote.Authent.Login

import android.content.Intent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.moneynote.Authent.ForgotPass.ForgotPass
import com.example.moneynote.Authent.ForgotPass.Register
import com.example.moneynote.TabbarHomePage.TabbarHomePageActivity

class LoginRouter {

    var activity: Login? = null

    fun showToast(err: String) {
        activity?.showErr(err)
    }

    fun doGotoForgotPasswordScreen() {
        activity?.hideProgressHub()

        if (this.activity == null) {
            return
        }
        val intent = Intent(this.activity, ForgotPass::class.java)
        this.activity?.startActivity(intent)
    }

    fun doGoRegisterScreen() {
        activity?.hideProgressHub()

        if (this.activity == null) {
            return
        }

        val intent = Intent(this.activity, Register::class.java)
        this.activity?.startActivity(intent)
    }

    fun doGoHomePage() {
        activity?.hideProgressHub()

        if (this.activity == null) {
            return
        }

        val intent = Intent(this.activity, TabbarHomePageActivity::class.java)
        this.activity?.startActivity(intent)
    }
}